//
// Created by Matias Gonzalez on 15/06/2020.
//


#include <iostream>
#include <string>
using namespace std;

//tipos de actividad
int t[10];
// valores
double v[5];
//nombres de los deportes
char nom[5][20];

//acumulador de actividades
int actAcum [10];

void inicializarArreglos(){
    for (int i = 0; i < 5; ++i) {
        t[i]=-1;
        v[i]=0;
        actAcum[i]=0;
    }
}
void cargarArreglos(){
    string nombre;
    int tipo=0;
    double valor=0.0;

    int error;

    for (int i = 0; i < 5 ; ++i) {


            printf("\n Escriba el nombre del deporte %d: " ,i+1);
            fflush(stdin); // limpia la memoria de teclado cout << endl << "Nombre: ";
            gets(nom[i]);
            do { error=0;
                    printf("\n Tipo de actividad: ");
                    cin >> tipo;
                    if (cin.fail()) {
                        cout << "\nIngrese un valor correcto" << endl;
                        error = 1;
                        cin.clear();
                        cin.ignore(80, '\n');
                    } else{
                        t[i]=tipo;
                    }
            }while(error == 1 || tipo < 0 || tipo > 9);

            do { error=0;
                printf("\n Valor de la actividad: ");
                cin >> valor;
                if (cin.fail()) {
                    cout << "\nIngrese un valor correcto" << endl;
                    error = 1;
                    cin.clear();
                    cin.ignore(80, '\n');
                }else{
                    v[i]=valor;
                }
            }while(error == 1);
    }
}
void mostrarArreglos(){

        for (int i = 0; i < 5; ++i) {
            printf("Deporte: %s - Tipo Actividad: %d - Valor de Actividad: $ %.2f\n",nom[i],t[i],v[i]);
        }

}

void ordenarValores(){

    for (int i = 0; i < 5-1 ; i++) {
        for (int j = i+1; j < 5 ; j++) {
            if(v[i] > v[j]){

                char aux1[20];
                strcpy(aux1, nom[i]);
                strcpy(nom[i], nom[j]);
                strcpy(nom[j], aux1);

                double auxValor = v[i];
                v[i] = v[j];
                v[j] = auxValor;

                int auxTipo = t[i];
                t[i] = t[j];
                t[j] = auxTipo;
            }
        }

    }

}
void ordenarAlfabetico(){

        for (int i = 0; i < 5 - 1; i++) {
            for (int j = i + 1; j < 5; j++) {
                if (strcmp(nom[i], nom[j]) > 0) {
                    char aux1[20];
                    strcpy(aux1, nom[i]);
                    strcpy(nom[i], nom[j]);
                    strcpy(nom[j], aux1);

                    double auxValor = v[i];
                    v[i] = v[j];
                    v[j] = auxValor;

                    int auxTipo = t[i];
                    t[i] = t[j];
                    t[j] = auxTipo;
                }
            }
        }


}
//vector de conteo
void arreglosConteo(){
    for (int i = 0; i < 5 ; ++i) {
        actAcum[t[i]]++;
    }
    cout <<"\n";
    for (int i = 0; i < 5 ; ++i) {
        cout <<"Actividades tipo "<< i << ": " << actAcum[i] << "\n";
    }

}
void puntoD(){

    ordenarValores();
    printf("Actividad con menor valor: Deporte: %s - Tipo Actividad: %d - Valor de Actividad: $ %.2f\n",nom[0],t[0],v[0]);

}
void puntoE(){
    int flag=0;
    char nombre[20];

        printf( "\n  Escriba el deporte que busca: ");
        cin >> nombre;
    for (int i = 0; i < 5; ++i) {
        int comparacion = strcmp(nom[i], nombre);
       if( comparacion == 0){
           printf("Deporte: %s - Tipo Actividad: %d - Valor de Actividad: $ %.2f\n",nom[i],t[i],v[i]);
           flag=1;
       }
    }
    if(flag == 0){
        printf("No se encontro deporte con el nombre: %s \n",nombre);
    }
}
void puntoF(){
    ordenarAlfabetico();

    int error;
    int flag=0;
    double arancel;
    do {
        error=0;
        printf( "\n  Escriba el arancel de referencia: ");
        cin >> arancel;
        if (cin.fail()) {
            cout << "Ingrese un valor correcto" << endl;
            error = 1;
            cin.clear();
            cin.ignore(80, '\n');
        }
    }while(error == 1);

    for (int i = 0; i < 5 ; ++i) {

        if(v[i] > arancel)
        {
            printf("Deporte: %s - Tipo Actividad: %d - Valor de Actividad: $ %.2f\n",nom[i],t[i],v[i]);
            flag=1;
        }

    }
    if(flag == 0){
        printf("No se encontro deporte que pague arancel mayor a: %f.2 \n",arancel);
    }
}
int main(){
    inicializarArreglos();
    int opcion;
    do
    {
        printf( "\n   Elija una dificultad: ");
        printf( "\n   1. Cargar Arreglos." );
        printf( "\n   2. Mostrar Actividades." );
        printf( "\n   3. Actividades disponibles." );
        printf( "\n   4. Punto d." );
        printf( "\n   5. Punto e." );
        printf( "\n   6. Punto f." );
        printf( "\n   7. Salir." );
        scanf( "%d", &opcion );

        switch ( opcion )
        {
            case 1:
                cargarArreglos();
                break;

            case 2:
                mostrarArreglos();
                break;

            case 3:
                arreglosConteo();
                break;

            case 4:
                puntoD();
                break;

            case 5:
                puntoE();
                break;

            case 6:
                puntoF();
                break;
            default:
                printf( "\n\n Opcion incorrecta! \n\n");
                break;
        }

    } while ( opcion != 7 );
    return 0;
}